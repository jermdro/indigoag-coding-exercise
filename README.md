# Indigo Ag Data Collector

The interface to this work is exactly as described in the original email.

```python
dc = DataCollector()
dc.add(1)
dc.add(3)
dc.add(100)
stats = dc.build_stats()

stats.less(99)
stats.greater(3)
stats.between(1, 100)

```

## Running the Code

You can run main by navigating to src/ and running `python3 main.py`.

You can run the tests with `python3 -m unittest`.

You can examine the performance of build stats by running `python3 StatsProfile.py`.

## Modified Counting Sort

This counting sort implementation is a O(U + H) sorting algorithm where U is the number of unique data points and H is the possible range for the data. 
Both U and H are O(N), because the number range, and thus the number of possible unique numbers, is known. While you could technically call this algorithm 
O(1), since both values are bound, that would gloss over the effect U and H have on time complexity, which I will dive into later. 

## Using an Array to Represent the Distribution

I used an array to represent the distribution. The ith cell of the array is equal to the number of data points at or below i. 
The array space scales linearly with the size of the possible range. Constant lookup time in the array is possible for all stats 
operations, even if the query value never occurred in the observed dataset.

## Tradeoffs of Counting Sort Approach

As mentioned previously, the counting sort algorithm is O(U + H). H is a high fixed costs that must be paid regardless of the number of unique 
data points. As the saturation (U/H) of the possible number range approaches 1, we don't see that substantial of an increase in time. 
The most likely explanation is that the sweep of the whole data point range combined with the fixed cost of uninteresting parts of `build_stats` 
still account for a large percentage of the time on such a small set of data.

![Performance](perf.png)

The above graph is for when H is equal to 1000. It's also worth pointing out that the profiling tools I used isn't great at this granularity of time. It does introduce 
some noise; however, the noise is even across all trials.

### Alternatives to Counting Sort

The counting sort plus array can be considered wasteful when H >> U. An alternative approach for representing sparse data sets would be using a Radix trie. 
Under these circumstances, Radix tries are compact and would be much quicker to build than a counting sort distribution. There are caveats to Radix trie's 
build stats performance. Mainly, the lookup time for a new node would be O(log(H)) and would likely not benefit from any caching locality since nodes are not 
stored adjacent to each other like entries in an array. 

While I considered building a Radix trie, I opted not to since they are complex and I already had a performant working solution. If we encountered this problem in the wild, 
you could decide between these two approaches by considering which operations you want to optimizes, the size of H and U, and whether the stats object should be 
persisted.

Thank you for taking the time to examine my code. 
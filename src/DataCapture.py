
import abc
from collections import defaultdict

from Stats import IStats, StatsFactory, StatsFactoryType

class IDataCapture(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def add(self, dataPoint):
        pass
    
    @abc.abstractmethod
    def build_stats(self) -> IStats:
        pass 

class DataCapture(IDataCapture):

    def __init__(
        self, 
        statsFactory: StatsFactoryType=StatsFactory,
        maxValue: int=1000,
        minValue: int=0):

        self._data = defaultdict(lambda: 0)
        self._factory = statsFactory
        self._min = minValue
        self._max = maxValue
        pass
    
    def add(self, dataPoint: int):
        assert type(dataPoint) == int
        if self._min > dataPoint:
            raise ValueError(
                '{} is smaller than min {}'.format(dataPoint, self._min))
        
        elif self._max < dataPoint: 
            raise ValueError(
                '{} is larger than max {}'.format(dataPoint, self._max))

        self._data[dataPoint] += 1

    def build_stats(self):
        return self._factory(self._data, self._min, self._max)

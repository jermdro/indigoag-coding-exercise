import unittest

from Stats import StatsFactoryType

# This Class was split out of the Stats tests because 
# these tests hold true regradless of the implementation
# of the distribution.
class DistributionEdgeCaseAndSanityTests:
    at1 = 45
    at10 = 55
    at100 = 1

    @classmethod
    def makeTestStats(cls, fact):
        return fact(
            {
                1: cls.at1,
                10: cls.at10,
                100: cls.at100
            },
            1, #min
            100 #max
        )

    @classmethod
    def run_less_test(cls, fact: StatsFactoryType, test: unittest.TestCase):
        s = cls.makeTestStats(fact)
        at1 = cls.at1
        at10 = cls.at10
        at100 = cls.at100

        # Below lower bound.
        test.assertEqual(s.less(-1), 0)
        # At lower bound.
        test.assertEqual(s.less(1), 0)
        # Somewhere in the middle
        test.assertEqual(s.less(2), at1)
        test.assertEqual(s.less(10), at1)
        # At upper bound.
        test.assertEqual(s.less(100), at1 + at10)
        # Above upper bound
        test.assertEqual(s.less(101), at1 + at10 + at100)
        test.assertEqual(s.less(1000), at1 + at10 + at100)

    @classmethod
    def run_greater_test(cls, fact: StatsFactoryType, test: unittest.TestCase):
        s = cls.makeTestStats(fact)
        at1 = cls.at1
        at10 = cls.at10
        at100 = cls.at100

        # Below lower bound.
        test.assertEqual(s.greater(-1), at1 + at10 + at100)
        # At lower bound.
        test.assertEqual(s.greater(1), at10 + at100)
        # Somewhere in the middle
        test.assertEqual(s.greater(2), at10 + at100)
        test.assertEqual(s.greater(9), at10 + at100)
        test.assertEqual(s.greater(10), at100)
        test.assertEqual(s.greater(11), at100)
        # At upper bound.
        test.assertEqual(s.greater(100), 0)
        # Above upper bound.
        test.assertEqual(s.greater(101), 0)

    @classmethod
    def run_between_test(cls, fact: StatsFactoryType, test: unittest.TestCase):
        s = cls.makeTestStats(fact)
        at1 = cls.at1
        at10 = cls.at10
        at100 = cls.at100
        # lb, ub < LB
        test.assertEqual(s.between(-1, 0), 0)
        # UB < lb, ub
        test.assertEqual(s.between(101, 102), 0)

        # lb,ub = LB
        test.assertEqual(s.between(1, 1), at1)

        #lb = LB, ub < UB
        test.assertEqual(s.between(1, 2), at1)
        test.assertEqual(s.between(1, 9), at1)
        test.assertEqual(s.between(1, 10), at1 + at10)

        #LB < lb, ub < UB
        test.assertEqual(s.between(2, 9), 0)
        test.assertEqual(s.between(2, 10), at10)
        test.assertEqual(s.between(2, 11), at10)

        #LB < lb < ub = UB
        test.assertEqual(s.between(9, 100), at10 + at100)
        test.assertEqual(s.between(10, 100), at10 + at100)
        test.assertEqual(s.between(11, 100), at100)

        # lb, ub = UB
        test.assertEqual(s.between(100, 100), at100)

        #lb = UB < ub
        test.assertEqual(s.between(100, 101), at100)
        test.assertEqual(s.between(100, 1000), at100)

        #UB < lb, ub
        test.assertEqual(s.between(101, 105), 0)
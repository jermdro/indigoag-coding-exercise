
import abc
from typing import Dict, Callable, List
import math

class IStats(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def less(self, v: int) -> int:
        pass
    
    @abc.abstractmethod
    def greater(self, v: int) -> int:
        pass

    @abc.abstractmethod
    def between(self, lower: int, upper: int) -> int:
        pass

class CountingSortArrayBasedStats(IStats):

    def __init__(self, data: Dict[int, int], mn, mx):
        
        self._min = mn
        self._max = mx
        numOfPossibleEntries = self._max - self._min + 1

        # Each cumuluative array entry will
        # state the number of elements at that element and to the left.
        self._cumulative = [0] * numOfPossibleEntries 
        for k in data.keys():
            self._set(k, data[k])

        runningTotal = 0

        for k in range(self._min, self._max + 1):
            runningTotal += self._get(k)
            self._set(k, runningTotal)

        self._total = runningTotal
        
    def _set(self, num, val):
        self._cumulative[num - self._min] = val

    def _get(self, num):
        return self._cumulative[num - self._min]

    def less(self, v):
        if v <= self._min: 
            return 0
        if v > self._max:
            return self._total

        #<= would use just v.
        return self._get(v - 1)
    
    def greater(self, v):
        if v > self._max:
            return 0
        if v < self._min:
            return self._total
        
        # Greater than or equal would be v - 1
        return self._total - self._get(v)

    def between(self, lower, upper):
        if lower > upper:
            raise ValueError('Mixed up lower and upper')
        return self._total - self.less(lower) - self.greater(upper)

StatsFactoryType = Callable[[Dict[int, int], int, int], IStats]

def StatsFactory(data, mn, mx):
    return CountingSortArrayBasedStats(data, mn, mx)
from random import random,sample
from collections import defaultdict
from timeit import timeit
from Stats import StatsFactory
import progressbar
import numpy as np
import matplotlib.pyplot as plt

def profileBuildStatsAtSaturation(
    saturationLevel: float,
    mn=0,
    mx=1000):

    assert(saturationLevel > 0 and saturationLevel <= 1)

    data = {}
    dataSetSize = int ((mx - mn + 1) * saturationLevel)
    dps = sample(range(mn, mx + 1), dataSetSize)
    for v in dps:
        data[v] = random() * mx + mn

    elapsed = timeit('StatsFactory(data,mn,mx)', globals={
        'data': data, 
        'mn': mn, 
        'mx': mx, 
        'StatsFactory': StatsFactory}, number=1)

    return elapsed

def profileBuildStats():
    levels = [.05, .1, .2, .5, .75, 1]
    results = defaultdict(lambda: [])
    subtrials = 1000

    with progressbar.ProgressBar(max_value=subtrials * len(levels)) as bar:
        for i in range(0, len(levels)):
            saturation = levels[i]

            for j in range(0, subtrials):
                bar.update(i * subtrials + j)
                elapsed = profileBuildStatsAtSaturation(saturation, 0, 1000)
                results[saturation].append(elapsed)


    data = [np.array(results[s]) for s in levels]
    plt.boxplot(
        data, 
        manage_xticks=False,
        patch_artist=True,
        showfliers=False)

    plt.title('The number of unique datapoint\'s impact on build_stats')
    plt.xticks(range(1,len(levels) + 1), levels)
    plt.xlabel('Saturation')
    plt.ylabel('Time (s)')
    plt.show()
    

if __name__ == "__main__":
    profileBuildStats()

import unittest

from DataCapture import DataCapture
from Stats import IStats

class DummyStats(IStats):
    def __init__(self):
        pass
    
    def less(self, v: int):
        return 0
    
    def greater(self, v: int):
        return 0
    
    def between(self, lower: int, upper: int) -> int:
        return 0

class DataCaptureTests(unittest.TestCase):

    def test_allValidPointsAreInBuildStatsCall(self):

        spyData, spyMin, spyMax = 0,0,0 

        def spy_factory(data, mn, mx):
            nonlocal spyData
            nonlocal spyMin 
            nonlocal spyMax 
            spyData, spyMin, spyMax = data, mn, mx #pylint: disable = unused-variable
            return DummyStats()

        dc = DataCapture(spy_factory)

        dc.add(1)
        dc.add(2)
        dc.add(1000)
        dc.add(0)
        dc.add(1)
        dc.build_stats()

        self.assertEqual(
            spyData,
            {
                1: 2,
                2: 1,
                1000: 1,
                0: 1
            }
        )
        self.assertEqual(spyMin, 0)
        self.assertEqual(spyMax, 1000)

    def test_defaultMinAndMaxRejectPoints(self):
        dc = DataCapture()
        with self.assertRaises(ValueError):
            dc.add(-1)
        with self.assertRaises(ValueError):
            dc.add(1001)




if __name__ == '__main__':
    unittest.main()
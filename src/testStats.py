
import unittest

from Stats import CountingSortArrayBasedStats
from DistributionTesting import DistributionEdgeCaseAndSanityTests

class CountSortArrayBasedStats(unittest.TestCase):

    def test_LessReturnsNumberOfElementsLessThan(self):
        DistributionEdgeCaseAndSanityTests.run_less_test(CountingSortArrayBasedStats, self)

    def test_GreaterReturnsNumberOfElementsGreaterThan(self):
        DistributionEdgeCaseAndSanityTests.run_greater_test(CountingSortArrayBasedStats, self)

    def test_BetweenReturnsNumberOfElementsBetweenInclusive(self):
        DistributionEdgeCaseAndSanityTests.run_between_test(CountingSortArrayBasedStats, self)
